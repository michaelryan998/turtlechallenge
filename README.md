# TurtleChallenge

Usage:

.\TurtleChallenge.exe game-settings.txt moves.txt

game-settings.txt Format:

    5,4
    0,1,N
    4,2
    1,1 3,1 3,3

	Line 1 is board size, width * height
	Line 2 is turtle starting coordinate + direction (N, S, E, W - North, South, East, West)
	Line 3 is exit / success position on board
	Line 4 is a space separated list of coordinates containing mines

moves.txt Format:

    MRMMMMRM
    MMRMMRR
    MRMRMR
    MRMMRMMRRRMM

Each line is a sequence of moves. 

	M means move one tile in current direction.
	R means rotate 90 degrees right.
