﻿using System;
using System.Collections.Generic;
using System.IO;
using TurtleChallenge.Models;

namespace TurtleChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 2)
                {
                    throw new ArgumentException("game-settings and moves files required.");
                }

                var gameSettings = Utilities.GetGameSettings(args[0]);

                if (!File.Exists(args[1]))
                {
                    throw new ArgumentException("moves file not found");
                }

                var moves = Utilities.GetMoveSequences(args[1]);

                PlayGame(gameSettings, moves);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
        }

        private static void PlayGame(GameModel gameModel, string[] moves)
        {
            for (int i = 0; i < moves.Length; i++)
            {
                var res = PlaySequence(gameModel, moves[i]);
                Console.WriteLine($"Sequence {i}:\t{res}");
                gameModel.Turtle.Reset();
            }
        }

        private static string PlaySequence(GameModel gameModel, string moveSequence)
        {
            foreach (var move in moveSequence)
            {
                if (move.Equals('R'))
                {
                    gameModel.Turtle.Rotate();
                }
                else
                {
                    gameModel.Turtle.Move();

                    var tPos = gameModel.Turtle.CurrentPosition;
                    if (tPos.X < 0 ||
                        tPos.Y < 0 ||
                        tPos.X > gameModel.Board.Width ||
                        tPos.Y > gameModel.Board.Height)
                    {
                        return "Invalid Move!";
                    }

                    var tType = gameModel.Board.BoardTiles[tPos.X, tPos.Y].TileAssignment;

                    switch (tType)
                    {
                        case TileType.Exit:
                            return "Success!";
                        case TileType.Mine:
                            return "Hit Mine!";
                    }
                }
            }
            return "Still in danger!";
        }
    }
}
