﻿using System;
using System.IO;
using TurtleChallenge.Models;

namespace TurtleChallenge
{
    internal class Utilities
    {
        private const char Move = 'M';
        private const char Rotate = 'R';

        private static Turtle GetTurtlePosition(string turtlePos)
        {
            var turtleStr = turtlePos.Split(',');
            if (turtleStr.Length != 3)
            {
                throw new ArgumentException("Invalid turtle position settings, must be in format X,Y,DIRECTION");
            }

            var turtlePosition = ParsePosition($"{turtleStr[0]},{turtleStr[1]}");
            var turtleDirection = (Direction)Enum.Parse(typeof(Direction), turtleStr[2]);

            return new Turtle(turtlePosition, turtleDirection);
        }

        public static string[] GetMoveSequences(string moveSequencesPath)
        {
            if (!File.Exists(moveSequencesPath))
            {
                throw new ArgumentException("moves file not found");
            }
            var moves = File.ReadAllLines(moveSequencesPath);

            foreach (var move in moves)
            {
                foreach (var c in move)
                {
                    if (!c.Equals(Move) && !c.Equals(Rotate))
                    {
                        throw new ArgumentException("Invalid moves file");
                    }
                }
            }

            return moves;
        }

        public static GameModel GetGameSettings(string gameSettingsPath)
        {
            if (!File.Exists(gameSettingsPath))
            {
                throw new ArgumentException("game-settings file not found");
            }

            var settings = File.ReadAllLines(gameSettingsPath);
            if (settings.Length != 4)
            {
                throw new ArgumentException("Invalid game-settings file");
            }

            var board = GetBoardSettings(settings[0], settings[2], settings[3]);
            var turtle = GetTurtlePosition(settings[1]);

            var game = new GameModel(turtle, board);

            return game;
        }

        private static Board GetBoardSettings(string boardSizeStr, string exitPosStr, string minePositions)
        {
            var boardSize = boardSizeStr.Split(',');

            if (boardSize.Length != 2)
            {
                throw new ArgumentException("Invalid game-settings file");
            }

            if (!int.TryParse(boardSize[0], out int boardWidth) ||
                !int.TryParse(boardSize[1], out int boardHeight))
            {
                throw new ArgumentException($"Error parsing board size ({boardSize[0]},{boardSize[1]})");
            }

            var exitPos = ParsePosition(exitPosStr);

            var mineStrs = minePositions.Split(' ');
            var mines = new Position[mineStrs.Length];

            for (var i = 0; i < mineStrs.Length; i++)
            {
                mines[i] = ParsePosition(mineStrs[i]);
            }

            return new Board(boardWidth, boardHeight, exitPos, mines);
        }
        public static Position ParsePosition(string position)
        {
            var coord = position.Split(',');
            if (coord.Length != 2)
            {
                throw new ArgumentException($"Error parsing position ({position})");
            }

            if (!int.TryParse(coord[0], out int x) || !int.TryParse(coord[1], out int y))
            {
                throw new ArgumentException($"Error parsing position ({coord[0]}, {coord[1]})");
            }

            return new Position(x, y);
        }
    }
}
