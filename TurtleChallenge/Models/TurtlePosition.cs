﻿namespace TurtleChallenge.Models
{
    class TurtlePosition : Position
    {
        public Direction Direction;

        public TurtlePosition(int x, int y, Direction direction) : base (x, y)
        {
            Direction = direction;
        }
    }
}
