﻿namespace TurtleChallenge.Models
{
    class GameModel
    {
        public Turtle Turtle;
        public Board Board;

        public GameModel(Turtle turtle, Board board)
        {
            Turtle = turtle;
            Board = board;
        }
    }
}
