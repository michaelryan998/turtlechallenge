﻿namespace TurtleChallenge.Models
{
    public enum Direction { N, S, E, W }

    class Turtle
    {
        private readonly TurtlePosition _origPosition;
        public TurtlePosition CurrentPosition;

        public Turtle(Position position, Direction direction)
        {
            _origPosition = new TurtlePosition(position.X, position.Y, direction);
            CurrentPosition = new TurtlePosition(position.X, position.Y, direction);
        }

        public void Reset()
        {
            CurrentPosition = new TurtlePosition(_origPosition.X, _origPosition.Y, _origPosition.Direction);
        }

        public void Move()
        {
            if (CurrentPosition.Direction == Direction.N)
            {
                CurrentPosition.Y--;
            } else if (CurrentPosition.Direction == Direction.E)
            {
                CurrentPosition.X++;
            } else if (CurrentPosition.Direction == Direction.S)
            {
                CurrentPosition.Y++;
            }
            else
            {
                CurrentPosition.X--;
            }
        }

        public void Rotate()
        {
            if (CurrentPosition.Direction == Direction.N)
            {
                CurrentPosition.Direction = Direction.E;
            } else if (CurrentPosition.Direction == Direction.E)
            {
                CurrentPosition.Direction = Direction.S;
            } else if (CurrentPosition.Direction == Direction.S)
            {
                CurrentPosition.Direction = Direction.W;
            }
            else
            {
                CurrentPosition.Direction = Direction.N;
            }
        }
    }
}
