﻿namespace TurtleChallenge.Models
{
    public enum TileType {
        Free,
        Mine,
        Exit
    }

    public class Tile
    {
        public TileType TileAssignment { get; set; } = TileType.Free;
    }
}
