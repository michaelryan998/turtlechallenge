﻿using System;

namespace TurtleChallenge.Models
{
    public class Board
    {

        public Tile[,] BoardTiles;
        public int Width, Height;

        public Board(int x, int y, Position exit, Position[] mines) {
            if (x < 0 || y < 0) {
                throw new ArgumentException("Invalid board size");
            }
            Width = x;
            Height = y;
            BoardTiles = InitBoard(x, y);

            if (exit.X > x || exit.Y > y || exit.X < 0 || exit.Y < 0) {
                throw new ArgumentException("Invalid exit co-ordinates");
            }
            BoardTiles[exit.X, exit.Y].TileAssignment = TileType.Exit;

            foreach (Position p in mines) {
                if (p.X < 0 || p.Y < 0 || p.X > x || p.Y > y) {
                    throw new ArgumentException("Invalid mine co-ordinates");
                }
                if (!BoardTiles[p.X, p.Y].TileAssignment.Equals(TileType.Free)) {
                    throw new ArgumentException("Tile previously assigned value");
                }
                BoardTiles[p.X, p.Y].TileAssignment = TileType.Mine;
            }
        }

        private Tile[,] InitBoard(int x, int y)
        {
            BoardTiles = new Tile[x,y];

            for (int i = 0; i < x; i++)
            {
                for (int g = 0; g < y; g++)
                {
                    BoardTiles[i, g] = new Tile();
                }
            }

            return BoardTiles;
        }
    }
}
